from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options


class Driver:

    def __init__(self):

        #options = webdriver.ChromeOptions()
        #options.add_argument("--window-size=1366,768")
        #options.add_argument("--start-maximized")
        options = Options()
        options.headless = True

        self.instance = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME,
            options = options
            )
        #self.instance.set_window_position(-1366, 0)


    def navigate(self, url):
        if isinstance(url, str):
            self.instance.get(url)
        else:
            raise TypeError("URL must be a string.")

    def switchToNewWindow(self):
        # New windows will be the last object in window_handles
        self.instance.switch_to.window(self.instance.window_handles[-1])

    def switchToMainWindow(self):
        print("Switching to main window")
        self.instance.switch_to.window(self.instance.window_handles[0])

    def switchToMainWindowAndClose(self):
        self.instance.close()
        self.switchToMainWindow()

    
