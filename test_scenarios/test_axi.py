# Version 0.5
# Date: 01.07.2019 12:00
# Created by: Zoran Stankovic

import pytest
import allure
import time

from page_objects.homepage import HomePage
from page_objects.logingooglepage import GoogleLoginPage
from page_objects.firstpage import FirstPage
from page_objects.connecttosapsystempage import ConnectToSAPSystemPage
from page_objects.sapcreateflowpage import SAPCreateFlowPage
from page_objects.selectdatasourcetypepage import SelectDataSourceTypePage
from page_objects.selectobjectpage import SelectObjectPage
from page_objects.configureobjectpage import ConfigureObjectPage
from page_objects.loaddataingooglebigquerypage import LoadDataInGoogleBigQueryPage

from webdriver import Driver

@pytest.fixture(scope="session")
def driver_init(request):
    web_driver = Driver()
    session = request.node
    #This will inject web_driver as self.driver in each class bellow
    for item in session.items:
        cls = item.getparent(pytest.Class)
        setattr(cls.obj, "driver", web_driver)
    #This is executed after testing is done
    yield
    time.sleep(5)
    web_driver.instance.quit()

@pytest.mark.usefixtures("driver_init")
class TestAXISAP001:

    #Ensure driver is always on main window when test starts
    #This is required due to Google login procedure which opens new browser
    #window
    @pytest.fixture(autouse=True)
    def _driver_to_default(self):
        self.driver.switchToMainWindow()

    @allure.step("Test home page components.")
    def test_home_page_components(self):
        home_page = HomePage(self.driver)
        home_page.navigate_to_homepage()
        home_page.validate_google_login_button_is_visible()
        home_page.validate_google_login_button_is_clickable()
        # ****** Just for testing ********
        home_page.click_google_login_button()
        # ******************************** 
        capmanager = pytest.config.pluginmanager.getplugin('capturemanager')

        capmanager.suspend_global_capture(in_=True)
        input("Please Login manually and hit Enter to continue...")
        capmanager.resume_global_capture()
        self.driver.navigate("https://demo.axi.ai")

    #@allure.step("Test Login with Google credentials {username} - {password} - {phone_number}.")
    #@pytest.mark.parametrize(('username', 'password', 'phone_number'), 
    #        [pytest.param('wrong', 'wrong', 'wrong', marks=pytest.mark.xfail(reason="Wrong username and password.", run=False)),
    #         pytest.param('', '', '', marks=pytest.mark.xfail(reason="Empty username and password.", run=False)),
    #         ('autotest@axi.ai', '\)9YqMjE', '7793350895')])
    #def test_login_with_google(self, username, password, phone_number):
    #    home_page = HomePage(self.driver)
    #    home_page.click_google_login_button()
    #    googleLoginPage = GoogleLoginPage(self.driver, username, password, phone_number)
    #    googleLoginPage.validate_google_username_field_is_visible()
    #    googleLoginPage.validate_google_next_login_button_is_visible()
    #    googleLoginPage.validate_google_next_login_button_is_clickable()
    #    googleLoginPage.enter_username(username)
    #    googleLoginPage.validate_google_password_field_is_visible()
    #    googleLoginPage.validate_google_password_next_button_is_visible()
    #    googleLoginPage.validate_google_password_next_button_is_clickable()
    #    googleLoginPage.enter_password(password)
    #    googleLoginPage.validate_google_verify_phone_is_visible()
    #    googleLoginPage.click_verify_phone_number_option()

    #    googleLoginPage.validate_google_phone_number_field_is_visible()
    #    googleLoginPage.validate_google_phone_number_next_button_is_visible()
    #    googleLoginPage.validate_google_phone_number_next_button_is_clickable()
    #    googleLoginPage.verify_phone_number(phone_number)

    @allure.step("Test first page components.")
    def test_first_page_components_and_option_selection(self):
        first_page = FirstPage(self.driver)
        first_page.validate_SAP_connect_option_is_visible()
        first_page.validate_SAP_connect_option_is_clickable()
        first_page.click_SAP_connect_option()

    @allure.step("Test SAP create flow page components.")
    def test_SAP_create_flow_page_components_and_option_selection(self):
        SAP_create_flow_page = SAPCreateFlowPage(self.driver)
        SAP_create_flow_page.validate_create_flow_option_is_visible()
        SAP_create_flow_page.validate_create_flow_option_is_clickable()
        SAP_create_flow_page.click_create_flow_option()
   
    @allure.step("Test Setup Connection with: {app_server} - {system_id} - {client} - {username} - {password}.")
    @pytest.mark.parametrize(('app_server', 'system_id', 'client', 'username', 'password'), 
            [
             pytest.param('wrong', 'wrong', 'wrong', 'wrong', 'wrong', marks=pytest.mark.xfail(reason="Wrong credentials.", run=True, strict=True)),
             pytest.param('', '', '', '', '', marks=pytest.mark.xfail(reason="Empty credentials.", run=True, strict=True)),
             ('sap.agilexi.net', 'BXD', '100', 'TESTUSERA', 'm7NYs\Sl[{!')])
    def test_connect_to_SAP_system_page(self, app_server, system_id, client, username, password):
        connect_to_SAP_system_page =  ConnectToSAPSystemPage(self.driver, app_server, system_id, client, username, password)
        connect_to_SAP_system_page.validate_app_server_field_is_visible()
        connect_to_SAP_system_page.validate_system_id_field_is_visible()
        connect_to_SAP_system_page.validate_client_field_is_visible()
        connect_to_SAP_system_page.validate_username_field_is_visible()
        connect_to_SAP_system_page.validate_password_field_is_visible()
            
        connect_to_SAP_system_page.enter_app_server(app_server)
        connect_to_SAP_system_page.enter_system_id(system_id)
        connect_to_SAP_system_page.enter_client(client)
        connect_to_SAP_system_page.enter_username(username)
        connect_to_SAP_system_page.enter_password(password)

        connect_to_SAP_system_page.validate_connect_button_is_visible()
        connect_to_SAP_system_page.validate_connect_button_is_clickable()
        connect_to_SAP_system_page.click_connect_button()

    @allure.step("Test Select Data Source Type page components.")
    def test_select_data_source_type_page_components_and_option_selection(self):
        select_data_source_type_page = SelectDataSourceTypePage(self.driver)
        select_data_source_type_page.validate_BEx_Query_option_is_enabled()
        select_data_source_type_page.click_BEx_Query_option()

        select_data_source_type_page.validate_next_button_is_visible()
        select_data_source_type_page.validate_next_button_is_clickable()

        select_data_source_type_page.click_next_button() 

    @allure.step("Test Select Object with {query_string}.")
    @pytest.mark.parametrize(('query_string'), 
            [
             pytest.param('wrong', marks=pytest.mark.xfail(reason="Wrong query string.", run=True, strict=True)),
             pytest.param('', marks=pytest.mark.xfail(reason="Empty query string.", run=True, strict=True)),
             ('QRY_TEST_A_001')
            ])
    def test_select_object_page_components_and_selection(self, query_string):
        select_object_page = SelectObjectPage(self.driver, query_string)       
        select_object_page.validate_search_field_is_visible()
        select_object_page.validate_next_button_is_visible()

        select_object_page.enter_query_string(query_string)
        select_object_page.validate_query_item_is_visible(query_string)
        select_object_page.click_query_item(query_string)

        select_object_page.validate_next_button_is_clickable()
        select_object_page.click_next_button()

    @allure.step("Test Configure Object page components.")
    def test_configure_object_page_components_and_selection(self):
        configure_object_page = ConfigureObjectPage(self.driver) 
        configure_object_page.click_show_technical_details()

        configure_object_page.click_characteristics()
        configure_object_page.check_number_of_selected_items()

        configure_object_page.click_key_figures()
        configure_object_page.check_number_of_selected_items()
        configure_object_page.validate_next_button_is_visible()
        configure_object_page.validate_next_button_is_clickable()
        configure_object_page.click_next_button()

    @allure.step("Test Load Data In Google Big Query page with: {project_id} - {dataset} - {table}.")
    @pytest.mark.parametrize(('project_id', 'dataset', 'table'),
                             [
                                 pytest.param('wrong', 'wrong', 'wrong',
                                              marks=pytest.mark.xfail(reason="Wrong data.", run=True)),
                                 pytest.param('', '', '',
                                              marks=pytest.mark.xfail(reason="Empty data.", run=True)),
                                 ('AXIAI-AUTOTEST-001', 'axiai_autotest_001_data', 'TBL_QRYTA_USERA_')])
    def test_load_data_in_google_big_query_page(self, project_id, dataset, table):
        load_data_in_google_big_query_page = LoadDataInGoogleBigQueryPage(self.driver, project_id, dataset, table)

        load_data_in_google_big_query_page.validate_gcp_authenticated_is_visible()

        load_data_in_google_big_query_page.validate_project_id_field_is_visible()
        load_data_in_google_big_query_page.validate_dataset_field_is_visible()
        load_data_in_google_big_query_page.validate_table_select_field_is_visible()

        load_data_in_google_big_query_page.enter_project_id(project_id)
        load_data_in_google_big_query_page.enter_dataset(dataset)
        load_data_in_google_big_query_page.enter_table(table)
        load_data_in_google_big_query_page.validate_create_new_table_field_is_visible()
        load_data_in_google_big_query_page.enter_create_new_table(table)

        load_data_in_google_big_query_page.validate_flow_name_field_is_visible()
        load_data_in_google_big_query_page.validate_finish_button_is_clickable()
        load_data_in_google_big_query_page.click_finish_button()