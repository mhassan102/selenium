from page_objects.base import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException

from datetime import datetime
import pytest
import allure
import time

class LoadDataInGoogleBigQueryPage(BasePage):
    # Class implements actions on Load Data In Google Big Query page

    gcp_authenticated_locator = (By.XPATH, "//span[text()='GCP authenticated']")

    project_id_field_locator = (By.ID, "project-select")
    dataset_field_locator = (By.ID, "dataset-select")
    table_select_field_locator = (By.ID, "table-select")
    flow_name_field_locator = (By.ID, "flow-name")
    create_new_table_select_field_locator = (By.ID, "table-name")

    create_button_locator = (By.ID, "create-button")
    finish_button_locator = (By.CSS_SELECTOR, "[class='btn btn--primary']")

    alert_locator = (By.CSS_SELECTOR, "[role='alertdialog']")
    results_table_locator = (By.CSS_SELECTOR, "[class='table results-table']")

    def __init__(self, driver, project_id, dataset, table):
        self.driver = driver
        self.project_id = project_id
        self.dataset = dataset
        self.table = table

    # --------------------------- VALIDATING VISIBILITY ETC. ---------------------------------------------
    @allure.step("Validate GCP authenticated field to be visible.")
    def validate_gcp_authenticated_is_visible(self):
        gcp_authenticated = self.wait_for_element_visibility(self.gcp_authenticated_locator)
        assert gcp_authenticated.is_displayed()

    @allure.step("Validate Project ID field to be visible.")
    def validate_project_id_field_is_visible(self):
        project_id_field = self.wait_for_element_visibility(self.project_id_field_locator)
        assert project_id_field.is_displayed()

    @allure.step("Validate Dataset field to be visible.")
    def validate_dataset_field_is_visible(self):
        dataset_field = self.wait_for_element_visibility(self.dataset_field_locator)
        assert dataset_field.is_displayed()

    @allure.step("Validate Table field to be visible.")
    def validate_table_select_field_is_visible(self):
        table_select_field = self.wait_for_element_visibility(self.table_select_field_locator)
        assert table_select_field.is_displayed()

    @allure.step("Validate Create Table (Table Name) field to be visible.")
    def validate_create_new_table_field_is_visible(self):
        create_new_table_field = self.wait_for_element_visibility(self.create_new_table_select_field_locator)
        assert create_new_table_field.is_displayed()

    @allure.step("Validate Flow Name field to be visible.")
    def validate_flow_name_field_is_visible(self):
        flow_name_field = self.wait_for_element_visibility(self.flow_name_field_locator)
        assert flow_name_field.is_displayed()

    @allure.step("Validate Finish button to be visible.")
    def validate_finish_button_is_visible(self):
        finish_button = self.driver.instance.find_element(*self.finish_button_locator)
        assert finish_button.is_displayed()

    # --------------------------- VALIDATING CLICKABILITY ETC. ---------------------------------------------

    @allure.step("Validate Finish button to be clickable.")
    def validate_finish_button_is_clickable(self):
        finish_button = self.wait_for_element_to_be_clickable(self.finish_button_locator)
        assert finish_button.is_enabled()

    # --------------------------- CLICKING -----------------------------------------------------------------
    @allure.step("Click on Finish option.")
    def click_finish_button(self):
        finish_button = self.wait_for_element_visibility(self.finish_button_locator)
        finish_button.click()

        # Asure that creation is ok
        try:
            self.wait_for_element_presence(self.results_table_locator, 40)
        except:
            pytest.xfail("Error Data Load.")

    # --------------------------- ENTERING DATA ------------------------------------------------------------

    @allure.step("Enter Project ID.")
    def enter_project_id(self, project_id):
        if project_id != '':
             # Asure that project is listed
             try:
                 project_id_field = self.driver.instance.find_element(*self.project_id_field_locator)
                 project_id_field.click()
                 specific_project_locator = (By.XPATH, "//span[text()='"+project_id+"']")
                 specific_project = self.wait_for_element_to_be_clickable(specific_project_locator)
                 specific_project.click()
             except:
                 pytest.xfail("Project ID: "+project_id+" not listed.")
             finally:
                 project_id_field.click()
        else:
             pytest.xfail("Empty Project ID.")
        return True

    @allure.step("Enter Dataset.")
    def enter_dataset(self, dataset):
        if dataset != '':
            # Asure that dataset is listed
            try:
                dataset_field = self.driver.instance.find_element(*self.dataset_field_locator)
                dataset_field.click()
                specific_dataset_locator = (By.XPATH, "//span[text()='" + dataset + "']")
                specific_dataset = self.wait_for_element_to_be_clickable(specific_dataset_locator)
                specific_dataset.click()
            except:
                pytest.xfail("Dataset: " + dataset + " not listed.")
            finally:
                dataset_field.click()
        else:
            pytest.xfail("Empty Dataset.")
        return True

    @allure.step("Enter Table.")
    def enter_table(self, table):
        if table != '':
            table_field = self.driver.instance.find_element(*self.table_select_field_locator)
            table_field.click()
            specific_table_locator = (By.XPATH, "//span[text()=' Create New Table ']")
            time.sleep(1)
            specific_table = self.wait_for_element_to_be_clickable(specific_table_locator)
            time.sleep(1)
            specific_table.click()
        else:
            pytest.xfail("Empty Table string.")
        return True

    @allure.step("Create New Table.")
    def enter_create_new_table(self, table):
        # Produce <YYMMDD> string
        table_suffix = datetime.today().strftime('%y%m%d')
        new_table_name = table + table_suffix
        if table != '':
            create_new_table_field = self.driver.instance.find_element(*self.create_new_table_select_field_locator)
            create_new_table_field.click()
            create_new_table_field.send_keys(Keys.CONTROL + "a")
            create_new_table_field.send_keys(Keys.DELETE)
            create_new_table_field.send_keys(new_table_name)

            # Click on Create button
            create_button = self.wait_for_element_to_be_clickable(self.create_button_locator)
            create_button.click()

            # Asure that creation is ok
            try:
                alert = self.wait_for_element_visibility(self.alert_locator)
                alert.click()
                time.sleep(1)
                pytest.xfail("Error creating BigQuery table. Possibly table name duplicated.s")
            except (TimeoutException, NoSuchElementException) as e:
                pass

        else:
            pytest.xfail("Empty Table string.")
        return True

