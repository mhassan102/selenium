from page_objects.base import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import pytest
import allure

# First page after login
class FirstPage(BasePage):
    # Class implements actions on the first page after login
    SAP_connect_option_locator = (By.ID, "container-1")

    def __init__(self, driver):
        self.driver = driver

    @allure.step("Validate SAP connect option to be visible.")
    def validate_SAP_connect_option_is_visible(self):
        SAP_connect_option = self.wait_for_element_visibility(self.SAP_connect_option_locator)
        assert SAP_connect_option.is_displayed()

    @allure.step("Validate SAP connect option to be clickable.")
    def validate_SAP_connect_option_is_clickable(self):
        SAP_connect_option = self.wait_for_element_to_be_clickable(self.SAP_connect_option_locator)
        assert SAP_connect_option.is_enabled()

    @allure.step("Click on SAP connect option.")
    def click_SAP_connect_option(self):
        self.click_on_element(self.SAP_connect_option_locator)

