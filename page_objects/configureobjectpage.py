from page_objects.base import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import pytest
import allure
import time

class ConfigureObjectPage(BasePage):
    # Class implements actions on Configure Object page
 
    show_technical_details_locator = (By.ID, "show-details")
    characteristics_locator = (By.CSS_SELECTOR, "[for='characteristics']")
    key_figures_locator = (By.CSS_SELECTOR, "[for='key-figures']")
    number_data_elements_selected_locator = (By.ID, "selected-count")
    next_button_locator = (By.ID, "finish")

    def __init__(self,driver):
        self.driver = driver
        
    #--------------------------- VALIDATING VISIBILITY ETC. ---------------------------------------------
    @allure.step("Validate Next button to be visible.")
    def validate_next_button_is_visible(self):
        button = self.wait_for_element_visibility(self.next_button_locator)
        assert button.is_displayed()

    #--------------------------- VALIDATING CLICKABILITY ETC. ---------------------------------------------
    @allure.step("Validate Next button to be clickable.")
    def validate_next_button_is_clickable(self):
        button = self.wait_for_element_to_be_clickable(self.next_button_locator)
        assert button.is_enabled()

    #--------------------------- CLICKING -----------------------------------------------------------------   
    @allure.step("Click on Show technical details checkbox.")
    def click_show_technical_details(self):
        show_technical_details_checkbox = self.wait_for_element_presence(self.show_technical_details_locator)
        clickable_div = show_technical_details_checkbox.find_element(By.XPATH, "./..")
        clickable_div.click()

    @allure.step("Click on Characteristics checkbox.")
    def click_characteristics(self):
        characteristics_checkbox = self.wait_for_element_presence(self.characteristics_locator)
        characteristics_checkbox.click()
        
    @allure.step("Click on Key figures checkbox.")
    def click_key_figures(self):
        key_figures = self.wait_for_element_presence(self.key_figures_locator)
        self.driver.instance.execute_script("arguments[0].scrollIntoView(true);", key_figures)
        time.sleep(2)
        self.driver.instance.execute_script("window.scrollBy(0,-70);") 
        time.sleep(1)
        key_figures.click()

    @allure.step("Click on Next button.")
    def click_next_button(self):
        self.click_on_element(self.next_button_locator)

    #--------------------------- ENTERING DATA ------------------------------------------------------------
    @allure.step("Enter Search query.")
    def enter_query_string(self, query_string):
        search_field = self.driver.instance.find_element(*self.search_field_locator)
        search_field.click()
        search_field.send_keys(Keys.CONTROL + "a")
        search_field.send_keys(Keys.DELETE)
        search_field.send_keys(query_string)
    
    #------------------------- CHECK NUMBER OF SELECTED ITEMS ----------------------------------------------
    @allure.step("Check number of selected Characteristics or Key Figures.")
    def check_number_of_selected_items(self):
        time.sleep(3)
        selected_characteristics = self.driver.instance.find_elements(By.CLASS_NAME, "query-dimension.selected")
        number_of_selected_characteristics = len(selected_characteristics)
        
        selected_figures = self.driver.instance.find_elements(By.CLASS_NAME, "query-key-figure.selected")
        number_of_selected_key_figures = len(selected_figures)

        #Showed Number data elements selected
        number_of_selected_items = int(self.driver.instance.find_element(*self.number_data_elements_selected_locator).text.strip())

        assert (number_of_selected_characteristics+number_of_selected_key_figures) == number_of_selected_items
