from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import WebDriverException
import time
from webdriver import Driver

import pytest
import allure

class BasePage():

    baseURL = "https://demo.axi.ai"
    waittime_def = 10

    def navigate_to_homepage(self):
        self.driver.navigate(self.baseURL)

    def wait_for_element_visibility(self, locator, waittime=waittime_def):
        return WebDriverWait(self.driver.instance, waittime).until(EC.visibility_of_element_located(locator))

    def wait_for_element_presence(self, locator, waittime=waittime_def):
        return WebDriverWait(self.driver.instance, waittime).until(EC.presence_of_element_located(locator))

    def wait_for_element_to_be_not_visibile(self, locator, waittime=waittime_def):
        return WebDriverWait(self.driver.instance, waittime).until(EC.invisibility_of_element_located(locator))

    def wait_for_element_to_be_clickable(self, locator, waittime=waittime_def):
        return WebDriverWait(self.driver.instance, waittime).until(EC.element_to_be_clickable(locator))

    @allure.step("Clicking on element: {locator}")
    def click_on_element(self, locator):
        self.wait_for_element_to_be_clickable(locator)
        self.driver.instance.find_element(*locator).click()

    @allure.step("Close Share your feedback if any.")
    def close_feedback(self):
        locator = (By.CSS_SELECTOR, "[class='prompt-close ng-fa-icon']")
        self.click_on_element(locator)

