from page_objects.base import BasePage
from selenium.webdriver.common.by import By

import allure

class HomePage(BasePage):
    # Class implements actions on homepage (login)
    google_login_button_locator = (By.CSS_SELECTOR, "[class*='login-button']")

    def __init__(self, driver):
        self.driver = driver

    @allure.step("Validate Google login button to be visible.")
    def validate_google_login_button_is_visible(self):
        button = self.wait_for_element_visibility(self.google_login_button_locator)
        assert button.is_displayed()

    @allure.step("Validate Google login button to be clickable.")
    def validate_google_login_button_is_clickable(self):
        button = self.wait_for_element_to_be_clickable(self.google_login_button_locator)
        assert button.is_enabled()

    @allure.step("Click on Google login button.")
    def click_google_login_button(self):
        self.click_on_element(self.google_login_button_locator)

