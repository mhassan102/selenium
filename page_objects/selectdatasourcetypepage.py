from page_objects.base import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import pytest
import allure

class SelectDataSourceTypePage(BasePage):
    # Class implements actions on the Select Data Source Type page
    BEx_Query_option_locator = (By.XPATH, "//span[contains(text(), 'BEx Query')]")
    next_button_locator = (By.ID, "next")

    def __init__(self, driver):
        self.driver = driver

    #--------------------------- VALIDATING VISIBILITY ETC. ---------------------------------------------
    @allure.step("Validate Next button to be visible.")
    def validate_next_button_is_visible(self):
        button = self.wait_for_element_visibility(self.next_button_locator)
        assert button.is_displayed()

    #--------------------------- VALIDATING CLICKABILITY ETC. ---------------------------------------------    
    @allure.step("Validate BEx Query option to be enabled.")
    def validate_BEx_Query_option_is_enabled(self):
        self.close_feedback()
        BEx_Query_option = self.wait_for_element_to_be_clickable(self.BEx_Query_option_locator)
        assert BEx_Query_option.is_enabled()
        
    @allure.step("Validate Next button to be clickable.")
    def validate_next_button_is_clickable(self):
        button = self.wait_for_element_to_be_clickable(self.next_button_locator)
        assert button.is_enabled()

    #--------------------------- CLICKING ------------------------------------------------------------------
    @allure.step("Click on BEx Query option.")
    def click_BEx_Query_option(self):
        self.click_on_element(self.BEx_Query_option_locator)    

    @allure.step("Click on Next button.")
    def click_next_button(self):
        self.click_on_element(self.next_button_locator)

