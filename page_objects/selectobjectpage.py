from page_objects.base import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import pytest
import allure
import time

class SelectObjectPage(BasePage):
    # Class implements actions on Select Object page
 
    search_field_locator = (By.ID, "search_input")
    choosed_query_field_locator = (By.ID, "system-id")
    # Selector query_item_selector_locator is dinamicaly generated so it is in method

    next_button_locator = (By.ID, "next")

    def __init__(self, driver, query_string):
        self.driver = driver
        self.query_string = query_string
        
    #--------------------------- VALIDATING VISIBILITY ETC. ---------------------------------------------
    @allure.step("Validate Search field to be visible.")
    def validate_search_field_is_visible(self):
        search_field = self.wait_for_element_visibility(self.search_field_locator)
        assert search_field.is_displayed()

    @allure.step("Validate Next button to be visible.")
    def validate_next_button_is_visible(self):
        button = self.wait_for_element_visibility(self.next_button_locator)
        assert button.is_displayed()

    @allure.step("Validate query item to be visible.")
    def validate_query_item_is_visible(self, query_string):
        #check number of results
        time.sleep(1)
        resulted_items = self.driver.instance.find_elements(By.CLASS_NAME, 'query-item')
        number_of_resulted_items = len(resulted_items)
        #print(number_of_resulted_items)
        if number_of_resulted_items == 1:
            query = '[title="'+ query_string + '"]'
            query_item_selector_locator = (By.CSS_SELECTOR, query)
            item = self.wait_for_element_visibility(query_item_selector_locator)
            assert item.is_displayed()
        elif number_of_resulted_items == 0:
            # No results
            pytest.xfail("Wrong query string.")
        else:
            # Too many results
            # Test should not come to this place ever
            pytest.xfail("Empty query string.")

    #--------------------------- VALIDATING CLICKABILITY ETC. ---------------------------------------------
    @allure.step("Validate Next button to be clickable.")
    def validate_next_button_is_clickable(self):
        button = self.wait_for_element_to_be_clickable(self.next_button_locator)
        assert button.is_enabled()

    #--------------------------- CLICKING -----------------------------------------------------------------   
    @allure.step("Click on query item.")
    def click_query_item(self, query_string):
        query = '[title="'+ query_string + '"]'
        query_item_selector_locator = (By.CSS_SELECTOR, query)
        item = self.wait_for_element_visibility(query_item_selector_locator)
        item_parent = item.find_element(By.XPATH, "./..")
        item_parent.click()

    @allure.step("Click on Next button.")
    def click_next_button(self):
        self.click_on_element(self.next_button_locator)

    #--------------------------- ENTERING DATA ------------------------------------------------------------
    @allure.step("Enter Search query.")
    def enter_query_string(self, query_string):
        if query_string!= '':
            search_field = self.driver.instance.find_element(*self.search_field_locator)
            search_field.click()
            search_field.send_keys(Keys.CONTROL + "a")
            search_field.send_keys(Keys.DELETE)
            search_field.send_keys(query_string)
            assert 1==1
        else:
           pytest.xfail("Empty query string.")

    
        

