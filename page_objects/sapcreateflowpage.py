from page_objects.base import BasePage
from selenium.webdriver.common.by import By

import allure

# First page after choosing SAP option
class SAPCreateFlowPage(BasePage):
    # Class implements actions on the first page after choosing SAP option
    create_flow_option_locator = (By.CSS_SELECTOR, "[routerlink='new']")

    def __init__(self, driver):
        self.driver = driver

    @allure.step("Validate Create Flow option to be visible.")
    def validate_create_flow_option_is_visible(self):
        create_flow_option = self.wait_for_element_visibility(self.create_flow_option_locator)
        assert create_flow_option.is_displayed()

    @allure.step("Validate Create Flow option to be clickable.")
    def validate_create_flow_option_is_clickable(self):
        create_flow_option = self.wait_for_element_to_be_clickable(self.create_flow_option_locator)
        assert create_flow_option.is_enabled()

    @allure.step("Click on Create Flow option.")
    def click_create_flow_option(self):
        self.click_on_element(self.create_flow_option_locator)

