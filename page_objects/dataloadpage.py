from page_objects.base import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import pytest
import allure
import time

class DataLoadPage(BasePage):
    # Class implements actions on Data Load page
 
    project_id_field_locator = (By.ID, "application-server")
    system_id_field_locator = (By.ID, "system-id")
    client_field_locator = (By.ID, "client")
    username_field_locator = (By.ID, "user")
    password_field_locator = (By.ID, "password")

    connect_button_locator = (By.CSS_SELECTOR, "[class='btn btn--primary']")

    def __init__(self, driver, app_server, system_id, client, username, password):
        self.driver = driver
        self.app_server = app_server
        self.system_id = system_id
        self.client = client
        self.username = username
        self.password = password
    
    #--------------------------- VALIDATING VISIBILITY ETC. ---------------------------------------------
    @allure.step("Validate Application Server field to be visible.")
    def validate_app_server_field_is_visible(self):
        app_server_field = self.wait_for_element_visibility(self.app_server_field_locator)
        assert app_server_field.is_displayed()

    @allure.step("Validate System ID field to be visible.")
    def validate_system_id_field_is_visible(self):
        system_id_field = self.wait_for_element_visibility(self.system_id_field_locator)
        assert system_id_field.is_displayed()

    @allure.step("Validate Client field to be visible.")
    def validate_client_field_is_visible(self):
        client_field = self.wait_for_element_visibility(self.client_field_locator)
        assert client_field.is_displayed()

    @allure.step("Validate Username field to be visible.")
    def validate_username_field_is_visible(self):
        username_field = self.wait_for_element_visibility(self.username_field_locator)
        assert username_field.is_displayed()

    @allure.step("Validate Password field to be visible.")
    def validate_password_field_is_visible(self):
        password_field = self.wait_for_element_visibility(self.password_field_locator)
        assert password_field.is_displayed()
    
    @allure.step("Validate Connect button to be visible.")
    def validate_connect_button_is_visible(self):
        connect_button = self.wait_for_element_visibility(self.connect_button_locator)
        assert connect_button.is_displayed() 

    #--------------------------- VALIDATING CLICKABILITY ETC. ---------------------------------------------
    @allure.step("Validate Connect button to be clickable.")
    def validate_connect_button_is_clickable(self):
        connect_button = self.wait_for_element_to_be_clickable(self.connect_button_locator)
        assert connect_button.is_enabled()

    #--------------------------- CLICKING -----------------------------------------------------------------
    @allure.step("Click on Connect option.")
    def click_connect_button(self):
        connect_button = self.wait_for_element_to_be_present(self.connect_button_locator)
        connect_button.click()

    #--------------------------- ENTERING DATA ------------------------------------------------------------
 
    @allure.step("Enter Application Server.")
    def enter_app_server(self, app_server):
        app_server_field = self.driver.instance.find_element(*self.app_server_field_locator)
        app_server_field.click()
        app_server_field.send_keys(Keys.CONTROL + "a")
        app_server_field.send_keys(Keys.DELETE)
        app_server_field.send_keys(app_server)

    @allure.step("Enter System ID.")
    def enter_system_id(self, system_id):
        system_id_field = self.driver.instance.find_element(*self.system_id_field_locator)
        system_id_field.click()
        system_id_field.send_keys(Keys.CONTROL + "a")
        system_id_field.send_keys(Keys.DELETE)
        system_id_field.send_keys(system_id)

    @allure.step("Enter Client.")
    def enter_client(self, client):
        client_field = self.driver.instance.find_element(*self.client_field_locator)
        client_field.click()
        client_field.send_keys(Keys.CONTROL + "a")
        client_field.send_keys(Keys.DELETE)
        client_field.send_keys(client)

    @allure.step("Enter Username.")
    def enter_username(self, username):
        username_field = self.driver.instance.find_element(*self.username_field_locator)
        username_field.click()
        username_field.send_keys(Keys.CONTROL + "a")
        username_field.send_keys(Keys.DELETE)
        username_field.send_keys(username)

    @allure.step("Enter Password.")
    def enter_password(self, password):
        password_field = self.driver.instance.find_element(*self.password_field_locator)
        password_field.click()
        password_field.send_keys(Keys.CONTROL + "a")
        password_field.send_keys(Keys.DELETE)
        password_field.send_keys(password)

    
        
