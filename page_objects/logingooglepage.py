from page_objects.base import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

import allure

class GoogleLoginPage(BasePage):
    # Class implements actions on Google login page
 
    google_username_field_locator = (By.ID, "identifierId")
    google_password_field_locator = (By.NAME, "password")
    google_phone_number_field_locator = (By.ID, "phoneNumberId")

    google_verify_phone_locator = (By.CSS_SELECTOR, "[data-challengetype='13']")

    google_next_login_button_locator = (By.ID, "identifierNext")
    google_password_next_button_locator = (By.ID, "passwordNext")
    google_phone_number_next_button_locator = (By.CSS_SELECTOR, "[tabindex='0']")

    google_country_list_locator = (By.ID, "countryList")
    google_phone_number_prefix_locator = (By.CSS_SELECTOR, "[data-value='gb']")

    def __init__(self, driver, username, password, phone_number):
        self.driver = driver
        self.driver.switchToNewWindow()
        self.username = username
        self.password = password
        self.phone_number = phone_number
    
    #--------------------------- VALIDATE VISIBILITY ETC. ---------------------------------------------
    @allure.step("Validate Google username field to be visible.")
    def validate_google_username_field_is_visible(self):
        google_username_field = self.wait_for_element_visibility(self.google_username_field_locator)
        assert google_username_field.is_displayed()

    @allure.step("Validate Google next login button to be visible.")
    def validate_google_next_login_button_is_visible(self):
        google_next_login_button = self.wait_for_element_visibility(self.google_next_login_button_locator)
        assert google_next_login_button.is_displayed()
   
    @allure.step("Validate Google password field to be visible.")
    def validate_google_password_field_is_visible(self):
        google_password_field = self.wait_for_element_visibility(self.google_password_field_locator)
        assert google_password_field.is_displayed()

    @allure.step("Validate Google submit login button to be visible.")
    def validate_google_password_next_button_is_visible(self):
        google_password_next_button = self.wait_for_element_visibility(self.google_password_next_button_locator)
        assert google_password_next_button.is_displayed()

    @allure.step("Validate Google verify phone number option to be visible.")
    def validate_google_verify_phone_is_visible(self):
        google_verify_phone = self.wait_for_element_visibility(self.google_verify_phone_locator)
        assert google_verify_phone.is_displayed()

    @allure.step("Validate Google phone_number field to be visible.")
    def validate_google_phone_number_field_is_visible(self):
        google_phone_number_field = self.wait_for_element_visibility(self.google_phone_number_field_locator)
        assert google_phone_number_field.is_displayed()

    @allure.step("Validate Google verify phone button to be visible.")
    def validate_google_phone_number_next_button_is_visible(self):
        google_phone_number_next_button = self.wait_for_element_visibility(self.google_phone_number_next_button_locator)
        assert google_phone_number_next_button.is_displayed()

    #--------------------------- VALIDATE CLICKABILITY ETC. ---------------------------------------------
    
    @allure.step("Validate Google next login button to be clickable.")
    def validate_google_next_login_button_is_clickable(self):
        google_next_login_button = self.wait_for_element_to_be_clickable(self.google_next_login_button_locator)
        assert google_next_login_button.is_enabled()

    @allure.step("Validate Google submit login button to be clickable.")
    def validate_google_password_next_button_is_clickable(self):
        google_password_next_button = self.wait_for_element_to_be_clickable(self.google_password_next_button_locator)
        assert google_password_next_button.is_enabled()

    @allure.step("Validate Google verify phone button to be clickable.")
    def validate_google_phone_number_next_button_is_clickable(self):
        google_phone_number_next_button = self.wait_for_element_to_be_clickable(self.google_phone_number_next_button_locator)
        assert google_phone_number_next_button.is_enabled()

    #--------------------------- CLICKING ---------------------------------------------
    @allure.step("Click on Google verify option.")
    def click_verify_phone_number_option(self):
        google_verify_phone = self.wait_for_element_to_be_present(self.google_verify_phone_locator)
        google_verify_phone.click()

    #--------------------------- Enter data ---------------------------------------------
    @allure.step("Enter username.")
    def enter_username(self, username):
        google_username_field = self.driver.instance.find_element(*self.google_username_field_locator)
        google_username_field.click()
        google_username_field.send_keys(Keys.CONTROL + "a")
        google_username_field.send_keys(Keys.DELETE)
        google_username_field.send_keys(username)
        self.click_on_element(self.google_next_login_button_locator)

    @allure.step("Enter password.")
    def enter_password(self, password):
        google_password_field = self.driver.instance.find_element(*self.google_password_field_locator)
        google_password_field.click()
        google_password_field.send_keys(Keys.CONTROL + "a")
        google_password_field.send_keys(Keys.DELETE)
        google_password_field.send_keys(password)
        self.click_on_element(self.google_password_next_button_locator)

    @allure.step("Verify phone number.")
    def verify_phone_number(self, phone_number):
        google_phone_number_field = self.driver.instance.find_element(*self.google_phone_number_field_locator)
        google_phone_number_field.click()
        google_phone_number_field.send_keys(Keys.CONTROL + "a")
        google_phone_number_field.send_keys(Keys.DELETE)
        google_phone_number_field.send_keys(phone_number)

        #TBD
        #google_country_list = self.driver.instance.find_element(*self.google_country_list_locator)
        #google_country_list.click()
        #google_gb_prefix = self.driver.instance.wait_for_element_visibility(self.google_phone_number_prefix_locator)
        #google_gb_prefix.click()

        self.click_on_element(self.google_phone_number_next_button_locator)
        